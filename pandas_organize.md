판다스 - 구조화된 데이터를 효과적으로 처리하고 저장할 수 있는 파이썬 라이브러리

Series - 넘파이의 array가 보강된 형태로 데이터와 인덱스를 가지고 있다.
딕셔너리로 데이터를 만든후 시리즈로도 변환가능

series = pd.Series([1,2,3,4], index = ['a', 'b', 'c', 'd'], name="Title")
print(series, "\n")

Series도 넘파이 array처럼 연산자 활용 가능
gdp_per_capita = country['gdp'] / country['population’]
country['gdp per capita’] = gdp_per_capita

데이터프레임 - 여러개의 series가 모여서 행과 열을 이룬 데이터

데이터 프레임 저장 
country.to_csv(“./country.csv”)
country.to_excel(“country.xlsx”)
읽기는 read_

.loc로 인덱싱, 슬라이싱  [:, :] 첫번째 :는 열을 두번째 :는 행을 슬라이싱한다.


.loc로 새 리스트 or 딕셔너리등을 활용해 데이터 프레임에 새 데이터 추가/ 수정 가능

새로운 컬럼 추가
dataframe[‘전화번호'] = np.nan 		#우선 np의 npn으로 값이 없는 형태로 추가
dataframe.loc[0, ‘전화번호’] = ‘01012341234’

데이터 프레임 연산
add ( + ), sub ( - ), mul ( * ), div ( / )
A.add(B, fill_value=0)
집계함수
sum, mean등의 집계함수를 사용할 수 있다.

정렬하기
sort_values('행')
df.sort_values('col1', ascending=False)


조건으로 검색하기

df.[df["A"] < 0.5 ) & (df["B"] > 0.3)]
df.query ("A < 0.5 and B > 0.3")

df["~"].str.contains ("~")
df.~.str.match("~")

함수로 데이터 처리하기
def square (x)
    return  x** 2
df['Num'].apply(square)
df["Square"] = df.Num.apply(lambda x: x ** 2)

def get_preprocess_phone (
  mapping_dict={
    "공 ": ",
    "일 ": ",
    "이 ": ",
    "삼 ": ",
    "사 ": ",
    "오 ": ",
    "-": "",
    ".": "",
  }
  for key, value in mapping_dict. items():
    phone = phone.replace(key, value)
  return phone
df["preprocess_phone"] = df ["~"].apply(get_preprocess_phone)

df.행이름.replace({"기존데이터":바뀔데이터, "기존데이터":바뀔데이터})

그룹으로 묶기
df.groupby('행이름').sum()
df.groupby (['행이름','행이름']).sum()

df.groupby('key').aggregate(['min', np. median , max])	->행별로 min, 평균, max값이 나옴
df.groupby('key').aggregate({'data1': 'min', 'data2': np. sum}) ->data1의 최소값, data2의 합계

def filter_by_mean(x):
  return x['data2']. mean() > 3
df.groupby('key').mean()	
df.groupby('key').filter(filter_by_mean)

그룹으로 묶은 후 함수 적용 가능
df.groupby('key').apply(lambda x: x.max() - x.min())

groupby로 묶인 데이터에서 key값으로 데이터를 가져올 수 있다.
df.groupby('시도').get_group('충남')  -> 충남으로 그룹한 후 충남으로 묶인 그룹들의 값을 보여준다.

행, 열 삭제 : drop() - 열 삭제시 axis = 1

컬럼에서 원하는 데이터 검색 후 그 컬럼의 값 변경 예시
gTomg = 1000
for col in food_nutrients_copy2.loc[: ,'단백질(g)':'카페인(㎎)']:
    if '㎎' in col:
        food_nutrients_copy2[col] = food_nutrients_copy2[col] * gTomg

(food_nutrients_copy2[nutrient_persent_list]>0.0).sum()
	-> 참 값의 갯수를 표현해준다.


