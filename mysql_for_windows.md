접속하기 위해서 우선 cmd에서 mysqlsh -u root -p 입력후 비밀번호 입력

sql 명령어를 사용하기 위해서 \sql을 입력하여 shell을 sql로 바꾼다

show databases ;로 데이터 베이스 생성 확인


CREATE DATABASE diet ; 데이터베이스 생성

USE diet ;              ->데이터베이스 사용

CREATE TABLE USER
(
	_id INT PRIMARY KEY AUTO_INCREMENT,
	id VARCHAR(32) NOT NULL,
	password VARCHAR(32) NOT NULL
) ENGINE=INNODB ;

복사붙여넣기 방법 -> 메모장에 작성한 코드를 오른쪽 마우스 클릭해서 복사하고 mysqlsh 프롬프트에서 다시 오른쪽 마우스 클릭
